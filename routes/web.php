<?php

use App\Events\eventTrigger;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/alertbox', function(){
    return view('listener');
});

Route::get('/fireevent', function(){
   event(new eventTrigger());
});

