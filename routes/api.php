<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use App\Events\eventTrigger;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/message', function(Request $request){

    App\Mensagem::create([
        'nome' => $request->input('message')
    ]);

    event(new eventTrigger());
});

Route::get('/messages', function(){
    return Response(App\Mensagem::latest()->first(), 200);
});

